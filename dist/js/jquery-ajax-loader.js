/**
 * jQuery Ajax Loader
 *
 * @version 1.0
 *
 * @author Rafał Różanka <https://www.github.com/rrozanka>
 */
(function($) {
    /**
     * Ajax loader.
     */
    $.ajaxLoader = function() {
        /**
         * Data attribute name.
         *
         * @type {string}
         */
        var dataAttributeName = 'overlay-wrappers-ids';

        /**
         * Show.
         *
         * @param element
         */
        this.show = function(element) {
            var id = _generateRandomId();

            $('body').append(_renderOverlayWrapper(id));

            var overlayWrapper = $('#' + id);
            var overlayWrapperAjaxLoader = overlayWrapper.find('.overlay-ajax-loader');

            overlayWrapper.css({
                top: element.offset().top,
                left: element.offset().left,
                width: element.outerWidth(),
                height: element.outerHeight()
            });

            overlayWrapperAjaxLoader.css({
                top: element.height() / 2,
                left: element.width() / 2
            });

            overlayWrapper.fadeIn();

            if (!_isElementVisible(overlayWrapperAjaxLoader[0])) {
                overlayWrapperAjaxLoader.css({
                    top: (window.innerHeight / 2) + $(window).scrollTop()
                });
            }

            if (element.data(dataAttributeName) == undefined) {
                element.data(dataAttributeName, []);
            }

            element.data(dataAttributeName).push(id);

            return id;
        };

        /**
         * Hide.
         *
         * @param element
         * @param id
         */
        this.hide = function(element, id) {
            if (id == undefined) {
                $.each(element.data(dataAttributeName), function(i, k) {
                    _hide(k);
                });

                return true;
            }

            _hide(id);
        };

        /**
         * Generate random id.
         *
         * @returns {string}
         *
         * @private
         */
        var _generateRandomId = function() {
            return 'overlay-loader-' + Math.random().toString(36).substr(2);
        };

        /**
         * Is element visible.
         *
         * @returns {boolean}
         *
         * @private
         */
        var _isElementVisible = function(element) {
            return (element.getBoundingClientRect().top >= 0)
                && (element.getBoundingClientRect().bottom <= window.innerHeight);
        };

        /**
         * Render overlay wrapper.
         *
         * @param id
         *
         * @returns {string}
         *
         * @private
         */
        var _renderOverlayWrapper = function(id) {
            return '<div id="' + id + '" class="overlay-wrapper">' +
                '<div class="loading-overlay"><!-- --></div>' +

                '<div class="overlay-ajax-loader">' +
                    '<div class="spinner">' +
                        '<div class="rect1"></div>' +
                        '<div class="rect2"></div>' +
                        '<div class="rect3"></div>' +
                        '<div class="rect4"></div>' +
                        '<div class="rect5"></div>' +
                    '</div>' +
                '</div>' +
            '</div>';
        };

        /**
         * Hide.
         *
         * @param id
         *
         * @private
         */
        var _hide = function(id) {
            var loadingOverlay = $('#' + id);

            loadingOverlay.fadeOut(400, function() {
                loadingOverlay.remove();
            });
        };
    };

    /**
     * Initialize jQuery plugin.
     *
     * @param options
     */
    $.fn.ajaxLoader = function(options) {
        var action;
        var id = undefined;

        if ($.type(options) == 'string') {
            action = options;
        } else {
            id = options.id;
            action = options.action;
        }

        var ajaxLoader = new $.ajaxLoader();

        switch (action) {
            case 'show':
                return ajaxLoader.show($(this));
            case 'hide':
                return ajaxLoader.hide($(this), id);
        }
    };
}(jQuery));
