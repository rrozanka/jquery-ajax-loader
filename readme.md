## jQuery Ajax Loader

* Show loading overlay
```
$('selector').ajaxLoader('show');

This function returns the overlay wrapper id.
```

* Hide loading overlay
```
To hide all attached overlay wrappers to given element use:
$('selector').ajaxLoader('hide');

To hide particular overlay wrapper use:
$('selector').ajaxLoader({
    action: 'hide',
    id: 'id-of-the-element-from-show-function'
});
```
